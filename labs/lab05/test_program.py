import pytest
from program import MCAsStr,bin16Todec
@pytest.mark.parametrize("input,expected", 
	[
    (['9001','3006','9001','1006','9002','0000'],("Disassembler",['READ', 'STO 6', 'READ', 'ADD 6', 'PRINT', 'STOP'])),
	(['9001','3003','9001','2001','9002','0000'],("Disassembler",['READ', 'STO 3', 'READ', 'SUB 1', 'PRINT', 'STOP'])),
    (['9001','3006','9010','1006','9003','0001'],("Error",['Line 3: Invalid machine code 9010', 'Line 5: Invalid machine code 9003', 'Line 6: Invalid machine code 0001']))
    ])
def test_MCAsStr(input,expected):
	assert MCAsStr(input) == expected
@pytest.mark.parametrize("input,expected", 
	[
    ('0001111101000010','8002'),
    ('0010001100101001','9001'),
    ('0000101110111110','3006'),
    ('0000001111101110','1006'),
    ('0000000000000000','0000'),
    ('0001001110001100','5004'),
	])
def test_bin16Todec(input,expected):
	assert str(bin16Todec(input)) == expected