def MCAsStr(code_list):
    ass_list = []
    error = []
    for i in range(len(code_list)):
        if (code_list[i] == '0000'):
            ass_list.append('STOP')
        elif (code_list[i] =='9001'):
            ass_list.append('READ')
        elif (code_list[i] =='9002'):
            ass_list.append('PRINT')
        elif (code_list[i][0]=='1'):
            ass_list.append('ADD '+str(code_list[i][3]))
        elif (code_list[i][0]=='2'):
            ass_list.append('SUB '+str(code_list[i][3]))
        elif (code_list[i][0]=='3'):
            ass_list.append('STO '+str(code_list[i][3]))
        elif (code_list[i][0]=='4'):
            ass_list.append('STA '+str(code_list[i][3]))
        elif (code_list[i][0]=='5'):
            ass_list.append('LOAD '+str(code_list[i][3]))
        elif (code_list[i][0]=='6'):
            ass_list.append('B '+str(code_list[i][3]))
        elif (code_list[i][0]=='7'):
            ass_list.append('BZ '+str(code_list[i][3]))
        elif (code_list[i][0]=='8'):
            ass_list.append('BP '+str(code_list[i][3]))
        else:
            error.append('Line '+str(i+1)+': Invalid machine code '+str(code_list[i]))
    return ("Disassembler",ass_list) if len(error) == 0 else ("Error",error)
def bin162dec(Ibin,n=0,p=15):
    return ((2**(p))*int(Ibin[n])) if (n==15 and p ==0) else ((2**(p))*int(Ibin[n]))+bin162dec(Ibin,n+1,p-1)
def bin16Todec(Ibin):
    return bin162dec(Ibin) if bin162dec(Ibin)!= 0 else '0000'
def lmc(INP_file):
    code_list = []
    code_listDec = []
    file_ = open(str(INP_file)+".lmc", "r")
    data = file_.read()
    code_list = [data[i:i+16] for i in range(0, len(data), 16)]
    for k in range(len(code_list)):
        code_listDec.append(str(bin16Todec(code_list[k])))
    ASSEM_LIST = MCAsStr(code_listDec)
    if (ASSEM_LIST[0] == 'Error'):
        for i in range(len(ASSEM_LIST[1])):
            print(ASSEM_LIST[1][i])
        return
    else :
        fw = open("Assembly_output.asm", "w")
        for i in range(len(ASSEM_LIST[1])):
            fw.write(ASSEM_LIST[1][i]+"\n")
        fw.close()
lmc("Xsource")