import re
#def lexical_analysis(str)===>token
def lexical_analysis(str_value):
    str_list = str_value.split(' ')
    token_list = []
    for i in range(len(str_list)):
        num = re.findall(r'[0-9]+', str_list[i]) 
        alp = re.findall(r'[a-zA-Z]+', str_list[i]) 
        opr = "+-*/"
        symb = {"(":"leftparenthesis",")":"rightparenthesis",":":"colon",";":"semicolon","{":"leftbraces","}":"rightbraces","[":"leftbrackets","]":"rightbrackets","`":"backquote","@":"at","^":"carat","=":"equal","!":"exclamationmark","%":"percent","~":"tilde","?":"questionmark",".":"period",",":"comma"}
        if(len(num)>1 and (opr[0] in str_list[i] or opr[1] in str_list[i] or opr[2] in str_list[i] or opr[3] in str_list[i]) ):
            continue
        elif(len(num)>0 and len(alp)>0 and (opr[0] in str_list[i] or opr[1] in str_list[i] or opr[2] in str_list[i] or opr[3] in str_list[i]) ):
            token = ("identifier",str_list[i])
            token_list.append(token)
        elif (str_list[i] in symb):
            token = (symb[str_list[i]])
            token_list.append(token)
        elif (str_list[i] in opr):
            if str_list[i] == opr[0]:
                token = ("plus",str_list[i])
            elif str_list[i] == opr[1]:
                token = ("minus",str_list[i])
            elif str_list[i] == opr[2]:
                token = ("times",str_list[i])
            elif str_list[i] == opr[3]:
                token = ("divided",str_list[i])
            token_list.append(token)
        elif(len(alp) > 0):
            token = ("identifier",str_list[i])
            token_list.append(token)
        elif (len(num) > 0):
            token = ("number",str_list[i])
            token_list.append(token)
        else:
            continue
    return token_list

#print(lexical_analysis('9876543210 abc d12 + ('))
#def semantic_analysis(int)===>boolean
def semantic_analysis(value):
    return True if (0 <= value <= 2147483647) else False

#def syntex_analysis(str)===>boolean
def syntax_analysis(value):
    Number=['0','1','2','3','4','5','6','7','8','9']
    for i in range(len(value)): 
        if(value[i] not in Number): 
            return False
    return True
#def TargetCodeGen(str,str,str)===>file
def TargetCodeGen(data,fileNameOutput,Target):
    fw = open("output_"+str(fileNameOutput)+"."+str(Target), "w")
    fw.write(data)
    fw.close()

"""implement simple semantic analysis to all programs"""
# Program 1.1 Src file to txt file.
def src2txt(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    list_Token = lexical_analysis(value)
    for item in list_Token:
        if(item[0]=="number"):
            if(syntax_analysis(item[1])):
                if (semantic_analysis(int(item[1]))):
                    TargetCodeGen(item[1],fileName,"txt")
                    return "successes"
                else:
                    return "Semantic_analysis | Value is out of the specified range"
            else:
                return "Syntax_analysis | Value not has only number"
        else:
            return "Lexical analysis | Value is neither a number nor an identifier"
    
# Program 1.2 Src file to py file and The output file should be executable by a Python 3 interpreter.
def src2py(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    list_Token = lexical_analysis(value)
    for item in list_Token:
        if(item[0]=="number"):
            if(syntax_analysis(item[1])):
                if (semantic_analysis(int(item[1]))):
                    statement  = "value = "+item[1]+"\n"
                    statement += "print(value)"
                    TargetCodeGen(statement,fileName,"py")
                    return "successes"
                else:
                    return "Semantic_analysis | Value is out of the specified range"
            else:
                return "Syntax_analysis | Value not has only number"
        else:
            return "Lexical analysis | Value is neither a number nor an identifier"
    
    
# Program 1.3 Src file to c file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2c(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    list_Token = lexical_analysis(value)
    for item in list_Token:
        if(item[0]=="number"):
            if(syntax_analysis(item[1])):
                if (semantic_analysis(int(item[1]))):
                    statement  = "#include <stdio.h> \n"
                    statement += "int main(){ \n"
                    statement += "int value = "+item[1]+";\n"
                    statement += 'printf(\"%d\",value); \n'
                    statement += "return 0;\n"
                    statement += "}"
                    TargetCodeGen(statement,fileName,"c")
                    return "successes"
                else:
                    return "Semantic_analysis | Value is out of the specified range"
            else:
                return "Syntax_analysis | Value not has only number"
        else:
            return "Lexical analysis | Value is neither a number nor an identifier"
    

# Program 1.4 Src file to cpp file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cpp(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    list_Token = lexical_analysis(value)
    for item in list_Token:
        if(item[0]=="number"):
            if(syntax_analysis(item[1])):
                if (semantic_analysis(int(item[1]))):
                    statement  = "#include <iostream> \n"
                    statement += "int main(){ \n"
                    statement += "int value="+item[1]+";\n"
                    statement += 'std::cout << value; \n'
                    statement += "return 0;\n"
                    statement += "}"
                    TargetCodeGen(statement,fileName,"cpp")
                    return "successes"
                else:
                    return "Semantic_analysis | Value is out of the specified range"
            else:
                return "Syntax_analysis | Value not has only number"
        else:
            return "Lexical analysis | Value is neither a number nor an identifier"

# Program 1.5 Src file to cs file and the executable program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cs(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    list_Token = lexical_analysis(value)
    for item in list_Token:
        if(item[0]=="number"):
            if(syntax_analysis(item[1])):
                if (semantic_analysis(int(item[1]))):
                    statement  = 'using System;\n'
                    statement += 'public class Program {\n'
                    statement += 'public static void Main(){\n'
                    statement += 'int value='+item[1]+';\n'
                    statement += 'Console.WriteLine(value);\n'
                    statement += '} } \n'
                    TargetCodeGen(statement,fileName,"cs")
                    return "successes"
                else:
                    return "Semantic_analysis | Value is out of the specified range"
            else:
                return "Syntax_analysis | Value not has only number"
        else:
            return "Lexical analysis | Value is neither a number nor an identifier"
    
"""
print(src2txt('Xsource'))
print(src2py('Xsource'))
print(src2c('Xsource'))
print(src2cpp('Xsource'))
print(src2cs('Xsource'))"""