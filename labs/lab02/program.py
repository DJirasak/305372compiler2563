"""implement simple semantic analysis to all programs"""
# Program 1.1 Src file to txt file.
def src2txt(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    if (0 <= int(value) <= 2147483647):
        fw = open("output_"+str(fileName)+".txt", "w")
        fw.write(value)
        fw.close()
    else:
        print("Value is out of the specified range")
        _ = input("")
    f.close()
    

# Program 1.2 Src file to py file and The output file should be executable by a Python 3 interpreter.
def src2py(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    if (0 <= int(value) <= 2147483647):
        fw = open("output_"+str(fileName)+".py", "w")
        statement  = "value = "+value+"\n"
        statement += "print(value)"
        fw.write(statement)
        fw.close()
    else:
        print("Value is out of the specified range")
        _ = input("")
    f.close()

# Program 1.3 Src file to c file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2c(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    if (0 <= int(value) <= 2147483647):
        fw = open("output_"+str(fileName)+".c", "w")
        statement  = "#include <stdio.h> \n"
        statement += "int main(){ \n"
        statement += "int value = "+value+";\n"
        statement += 'printf(\"%d\",value); \n'
        statement += "return 0;\n"
        statement += "}"
        fw.write(statement)
        fw.close()
    else:
        print("Value is out of the specified range")
        _ = input("")
    f.close()

# Program 1.4 Src file to cpp file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cpp(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    if (0 <= int(value) <= 2147483647):
        fw = open("output_"+str(fileName)+".cpp", "w")
        statement  = "#include <iostream> \n"
        statement += "int main(){ \n"
        statement += "int value="+value+";\n"
        statement += 'std::cout << value; \n'
        statement += "return 0;\n"
        statement += "}"
        fw.write(statement)
        fw.close()
    else:
        print("Value is out of the specified range")
        _ = input("")
    f.close()

# Program 1.5 Src file to cs file and the executable program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cs(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    if (0 <= int(value) <= 2147483647):
        fw = open("output_"+str(fileName)+".cs", "w")
        statement  = 'using System;\n'
        statement += 'public class Program {\n'
        statement += 'public static void Main(){\n'
        statement += 'int value='+value+';\n'
        statement += 'Console.WriteLine(value);\n'
        statement += '} } \n'
        fw.write(statement)
        fw.close()
    else:
        print("Value is out of the specified range")
        _ = input("")
    f.close()
    

src2txt('Xsource')
src2py('Xsource')
src2c('Xsource')
src2cpp('Xsource')
src2cs('Xsource')