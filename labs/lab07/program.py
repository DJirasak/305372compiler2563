def dump(Arr_REG,Arr_ASM,AC,PC,IR):
    _str  = "+--------------------------------------------------------------------------+\n"
    _str += "  REGISTERS:\n"
    _str += "       Accumulator (AC): "+str(AC)+"\n"
    _str += "       Program Counter (PC): "+str(PC)+"\n"
    _str += "       Instruction Register (IR): "+str(IR)+"\n"
    _str += "       Mnemonic: "+str(Arr_ASM[PC])+"\n\n"
    _str += "  MEMORY:\n"
    _str += "                0     1     2     3     4     5     6     7     8     9\n"
    for i in range(0,len(Arr_REG),10):
        if i < 10:
            _str += "       "+str(i)+"     "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
        elif i < 100:
            _str += "       "+str(i)+"    "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
        elif i < 1000:
            _str += "       "+str(i)+"   "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
        else:
            _str += "       "+str(i)+"  "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
    _str += "\n+--------------------------------------------------------------------------+"      
    print(_str)
def getToken(Arr_asm):
    Tk_code = []
    Error_arr = ['ERROR']
    add_Mask = {}
    op = ["STOP","DAT","ASS","SUB","STO","STA","LOAD","B","BZ","BP","READ","PRINT"]
    for m in range(len(Arr_asm)):
        if Arr_asm[m][0] not in op :
            add_Mask[str(Arr_asm[m][0])] = m
        elif (Arr_asm[m][0] in op ) or (Arr_asm[m][0] == ";"):
            continue
    for i in range(len(Arr_asm)):
        J_temp = float("inf")
        for j in range(len(Arr_asm[i])):
            if ((j == J_temp) or (Arr_asm[i][j] in add_Mask)):
                continue
            else:
                if Arr_asm[i][j] != ";":
                    if Arr_asm[i][j] == "STOP":
                        Tk_code.append((i,"ASM","STOP","0000"))
                    elif Arr_asm[i][j] == "DAT":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","DAT","000"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","DAT","00"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","DAT","0"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","DAT","000"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","DAT","00"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","DAT","0"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "ADD":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","ADD","100"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","ADD","10"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","ADD","1"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","ADD","100"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","ADD","10"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","ADD","1"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "SUB":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","SUB","200"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","SUB","20"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","SUB","2"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","SUB","200"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","SUB","20"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","SUB","2"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "STO":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","STO","300"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","STO","30"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","STO","3"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","STO","300"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","STO","30"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","STO","3"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "STA":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","STA","400"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","STA","40"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","STA","4"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","STA","400"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","STA","40"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","STA","4"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "LOAD":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","LOAD","500"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","LOAD","50"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","LOAD","5"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","LOAD","500"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","LOAD","50"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","LOAD","5"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "B":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","B","600"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","B","60"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","B","6"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","B","600"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","B","60"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","B","6"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "BZ":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","BZ","700"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","BZ","70"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","BZ","7"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","BZ","700"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","BZ","70"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","BZ","7"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "BP":
                        if(Arr_asm[i][j+1] not in add_Mask):
                            if len(str(Arr_asm[i][j+1]))==1:
                                Tk_code.append((i,"ASM","BP","800"+str(Arr_asm[i][j+1])))
                            elif len(str(Arr_asm[i][j+1]))==2:
                                Tk_code.append((i,"ASM","BP","80"+str(Arr_asm[i][j+1])))
                            else:
                                Tk_code.append((i,"ASM","BP","8"+str(Arr_asm[i][j+1])))
                            J_temp = j+1
                        else:
                            tmpAdd = add_Mask[str(Arr_asm[i][j+1])]
                            if len(str(tmpAdd))==1:
                                Tk_code.append((i,"ASM","BP","800"+str(tmpAdd)))
                            elif len(str(tmpAdd))==2:
                                Tk_code.append((i,"ASM","BP","80"+str(tmpAdd)))
                            else:
                                Tk_code.append((i,"ASM","BP","8"+str(tmpAdd)))
                            J_temp = j+1
                    elif Arr_asm[i][j] == "READ":
                        Tk_code.append((i,"ASM","READ","9001"))
                    elif Arr_asm[i][j] == "PRINT":
                        Tk_code.append((i,"ASM","PRINT","9002"))
                    else:
                        errLine = 'Line '+str(i+1)+': Invalid Assembly Code \"'+str(Arr_asm[i][j])+'\" '
                        Error_arr.append(errLine)
                else:
                    break
    if len(Error_arr)>1:
        return Error_arr
    else:
        return Tk_code

def cls_cmt(Arr_CL):
    Arr_newCL=[]
    for i in range(len(Arr_CL)):
        cl = []
        for j in range(len(Arr_CL[i])):
            if Arr_CL[i][j] == ";":
                break
            else:
                cl.append(Arr_CL[i][j])
        Arr_newCL.append(cl)
    return Arr_newCL
def loadAsm(INP_file="source"):
    code_line = []
    file_ = open(str(INP_file)+".asm", "r")
    data = file_.read()
    _code_line = [data.split("\n") for i in range(0, len(data))][0]
    for i in range(len(_code_line)):
        tmp1 = _code_line[i].split(" ")
        tmp2 = list(filter(lambda x : x != '', tmp1))
        code_line.append(tmp2)
        NewCodeLine = list(filter(lambda z : z != [], cls_cmt(code_line)))
    Token_code = getToken(NewCodeLine)
    return Token_code
def load(INP_file="source"):
    resultArr = [[],[]]
    ListTK = loadAsm(INP_file)
    if ListTK[0] == "ERROR":
        print("Found error!!!")
        for i in range(1,len(ListTK)):
            print(" |-----+",ListTK[i])
        return
    else:
        for j in range(len(ListTK)):
            resultArr[1].append(ListTK[j][2])
            resultArr[0].append(ListTK[j][3])
        print("Data loaded successfully...")
        return (resultArr)
def step(Arr_REG,AC,PC,IR):
    RESULT=[AC,PC,IR,Arr_REG]
    if Arr_REG[PC][0] in ['0','1','2','3','4','5','6','7','8','9']:
        RESULT[1] += 1
    if ((Arr_REG[PC][0] == "0") and (Arr_REG[PC] != "0000")):
        Arr_REG[int(Arr_REG[PC][1:])] = Arr_REG[PC]
    if Arr_REG[PC] == "9001":
        AC = input(" 9001 | Please, enter value : ")
        RESULT[0] = AC
    if Arr_REG[PC] == "9002":
        print("Accumulator (AC): "+str(AC)+"")
    if Arr_REG[PC][0] == "1":
        contents = Arr_REG[int(Arr_REG[PC][1:])]
        _AC = int(AC) + int(contents)
        RESULT[0] = str(_AC)
    if Arr_REG[PC][0] == "2":
        contents = Arr_REG[int(Arr_REG[PC][1:])]
        _AC = int(AC) - int(contents)
        RESULT[0] = str(_AC)
    if Arr_REG[PC][0] == "3":
        if len(str(AC))==1:
            AC = "000"+str(AC)
        elif len(str(AC))==2:
            AC = "00"+str(AC)
        elif len(str(AC))==3:
            AC = "0"+str(AC)
        else:
            AC = str(AC)
        Arr_REG[int(Arr_REG[PC][1:])] = AC
        RESULT[3] = Arr_REG
    if Arr_REG[PC][0] == "4":
        _AC = AC[2:]
        if len(str(_AC))==1:
            _AC = "000"+str(_AC)
        elif len(str(_AC))==2:
            _AC = "00"+str(_AC)
        elif len(str(_AC))==3:
            _AC = "0"+str(_AC)
        else:
            _AC = str(_AC)
        Arr_REG[int(Arr_REG[PC][1:])-1] = _AC
        RESULT[3] = Arr_REG
    if Arr_REG[PC][0] == "5":
        _AC = Arr_REG[int(Arr_REG[PC][1:])]
        RESULT[0] = _AC
    if Arr_REG[PC][0] == "6":
        address = int(Arr_REG[PC][1:])
        RESULT[1] = int(address)
    elif Arr_REG[PC][0] == "7":
        if (int(AC) == 0):
            address = int(Arr_REG[PC][1:])
            RESULT[1] = int(address)
    elif Arr_REG[PC][0] == "8":
        if (int(AC) >= 0):
            address = int(Arr_REG[PC][1:])
            RESULT[1] = int(address)
    RESULT[2] = Arr_REG[RESULT[1]-1]
    return RESULT
#Initial 
size = 30
arr_REG=['0000']*size
arr_Asm=[""]
cmd_Program = 1
ac_reg = '0000'
pc_reg = 0
ir_reg = 0
 #Loop Program
print("*** Welcome to an Extended ELMC ASSEMBLER! ***")
while(cmd_Program):
    _CMD_inp = input("LMC ?> ")
    CMD_inp = _CMD_inp.lower()
    if(CMD_inp == "quit" or CMD_inp == "exit"):
        cmd_Program = 0
    elif(CMD_inp == "dump"):
        dump(arr_REG,arr_Asm,ac_reg,pc_reg,ir_reg)
    elif(CMD_inp == "step"):
        result = step(arr_REG,ac_reg,pc_reg,ir_reg)
        if(arr_Asm[pc_reg] != "STOP"):
            ac_reg = result[0]
            pc_reg = result[1]
            ir_reg = result[2]
            arr_REG = result[3]
    elif(CMD_inp == "run"):
        ac_reg = '0000'
        pc_reg = 0
        ir_reg = 0
        if len(arr_Asm)>1:
            while(True):
                if (arr_Asm[pc_reg] != "STOP"):
                    result = step(arr_REG,ac_reg,pc_reg,ir_reg)
                    ac_reg = result[0]
                    pc_reg = result[1]
                    ir_reg = result[2]
                    arr_REG = result[3]
                elif (arr_Asm[pc_reg] == "STOP"):
                    result = step(arr_REG,ac_reg,pc_reg,ir_reg)
                    print('Run Complete!!!')
                    break
        else:
            print('Please load data!!!')
    elif(CMD_inp == "load"):
        result = load()
        for l in range(len(result[0])):
            arr_REG[l] = result[0][l]
        arr_Asm += result[1]
    elif(CMD_inp == "reset"):
        arr_REG=['0000']*size
        arr_Asm=[""]
        ac_reg = 0
        pc_reg = 0
        ir_reg = 0
    else:
        print("'"+CMD_inp+"' is not recognized as an internal or external command\n,operable program or batch file.")
        continue