        READ
        PRINT   ; print the ACC
LOOP    BZ QUIT ; Label this address as LOOP
                ; If ACC is 0, jump to the address labeled QUIT
        SUB ONE     ; Subtract ACC with the value stored at address ONE
        PRINT   ; Print the content of ACC
        B LOOP  ; Jump unconditionally to the address labeled LOOP
QUIT    STOP    ; Label this memory address as QUIT
ONE     DAT 1   ; Label the address as ONE
                ; Store the value 1 in this memory address