# Program 1.1 Src file to txt file.
def src2txt(fileName):
    f = open(str(fileName)+".src", "r")
    fw = open("output_"+str(fileName)+".txt", "w")
    fw.write(f.read())
    f.close()
    fw.close()

# Program 1.2 Src file to py file and The output file should be executable by a Python 3 interpreter.
def src2py(fileName):
    f = open(str(fileName)+".src", "r")
    fw = open("output_"+str(fileName)+".py", "w")
    extension="print('"+str(f.read())+"')"
    fw.write(extension)
    f.close()
    fw.close()

# Program 1.3 Src file to c file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2c(fileName):
    f = open(str(fileName)+".src", "r")
    fw = open("output_"+str(fileName)+".c", "w")
    extension  = "#include <stdio.h> \n"
    extension += "void main(){ \n"
    extension += 'printf(\"'+str(f.read())+'\"); \n'
    extension += "}"
    fw.write(extension)
    f.close()
    fw.close()

# Program 1.4 Src file to cpp file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cpp(fileName):
    f = open(str(fileName)+".src", "r")
    fw = open("output_"+str(fileName)+".cpp", "w")
    extension  = "#include <iostream> \n"
    extension += "void main(){ \n"
    extension += 'std::cout <<\"'+str(f.read())+'\"; \n'
    extension += "}"
    fw.write(extension)
    f.close()
    fw.close()

# Program 1.5 Src file to cs file and the executable program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cs(fileName):
    f = open(str(fileName)+".src", "r")
    fw = open("output_"+str(fileName)+".cs", "w")
    extension  = 'using System;\n'
    extension += 'public class Program {\n'
    extension += 'public static void Main(){\n'
    extension += 'Console.WriteLine("'+str(f.read())+'");\n'
    extension += '} } \n'
    fw.write(extension)
    f.close()
    fw.close()

src2txt('source')
src2py('source')
src2c('source')
src2cpp('source')
src2cs('source')