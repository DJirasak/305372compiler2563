#Import
#Function definition
def dump(Arr_REG,Arr_ASM,AC,PC,IR):
    _str  = "+--------------------------------------------------------------------------+\n"
    _str += "  REGISTERS:\n"
    _str += "       Accumulator (AC): "+str(AC)+"\n"
    _str += "       Program Counter (PC): "+str(PC)+"\n"
    _str += "       Instruction Register (IR): "+str(IR)+"\n"
    _str += "       Mnemonic: "+str(Arr_ASM[PC-1])+"\n\n"
    _str += "  MEMORY:\n"
    _str += "                0     1     2     3     4     5     6     7     8     9\n"
    for i in range(0,len(Arr_REG),10):
        if i < 10:
            _str += "       "+str(i)+"     "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
        elif i < 100:
            _str += "       "+str(i)+"    "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
        elif i < 1000:
            _str += "       "+str(i)+"   "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
        else:
            _str += "       "+str(i)+"  "+str(Arr_REG[i])+"  "+str(Arr_REG[i+1])+"  "+str(Arr_REG[i+2])+"  "+str(Arr_REG[i+3])+"  "+str(Arr_REG[i+4])+"  "+str(Arr_REG[i+5])+"  "+str(Arr_REG[i+6])+"  "+str(Arr_REG[i+7])+"  "+str(Arr_REG[i+8])+"  "+str(Arr_REG[i+9])+"\n"
    _str += "\n+--------------------------------------------------------------------------+"      
    print(_str)
def MCAsStr(code_list):
    ass_list = []
    error = []
    for i in range(len(code_list)):
        if (code_list[i] == '0000'):
            ass_list.append('STOP')
        elif (code_list[i] =='9001'):
            ass_list.append('READ')
        elif (code_list[i] =='9002'):
            ass_list.append('PRINT')
        elif (code_list[i][0]=='1'):
            ass_list.append('ADD '+str(int(code_list[i][1:])))
        elif (code_list[i][0]=='2'):
            ass_list.append('SUB '+str(int(code_list[i][1:])))
        elif (code_list[i][0]=='3'):
            ass_list.append('STO '+str(int(code_list[i][1:])))
        elif (code_list[i][0]=='4'):
            ass_list.append('STA '+str(int(code_list[i][1:])))
        elif (code_list[i][0]=='5'):
            ass_list.append('LOAD '+str(int(code_list[i][1:])))
        elif (code_list[i][0]=='6'):
            ass_list.append('B '+str(int(code_list[i][1:])))
        elif (code_list[i][0]=='7'):
            ass_list.append('BZ '+str(int(code_list[i][1:])))
        elif (code_list[i][0]=='8'):
            ass_list.append('BP '+str(int(code_list[i][1:])))
        else:
            error.append('Line '+str(i+1)+': Invalid machine code '+str(code_list[i]))
    return ("Disassembler",ass_list) if len(error) == 0 else ("Error",error)
def load(Arr_REG,INP_file="source"):
    file_ = open(str(INP_file)+".lmc", "r")
    data = file_.read()
    _code_list = [data.split("\n") for i in range(0, len(data), 16)]
    code_list = _code_list[0]
    ASSEM_LIST = MCAsStr(code_list)
    if (ASSEM_LIST[0] == 'Error'):
        print("Found error!!!")
        for i in range(len(ASSEM_LIST[1])):
            print(" |-----+",ASSEM_LIST[1][i])
        return
    else :
        print("Data loaded successfully...")
        for j in range(len(ASSEM_LIST[1])):
            Arr_REG[j] = code_list[j]
        return (Arr_REG,ASSEM_LIST[1])
def step(Arr_REG,AC,PC,IR):
    RESULT=[AC,PC,IR,Arr_REG]
    if Arr_REG[PC][0] in ['0','1','2','3','4','5','6','7','8','9']:
        RESULT[1] += 1
    if Arr_REG[PC] == "9001":
        AC = input(" 9001 | Please, enter value : ")
        RESULT[0] = AC
    if Arr_REG[PC] == "9002":
        print("Accumulator (AC): "+str(AC)+"")
    if Arr_REG[PC][0] == "1":
        contents = Arr_REG[int(Arr_REG[PC][1:])]
        _AC = int(AC) + int(contents)
        RESULT[0] = str(_AC)
    if Arr_REG[PC][0] == "2":
        contents = Arr_REG[int(Arr_REG[PC][1:])]
        _AC = int(AC) - int(contents)
        RESULT[0] = str(_AC)
    if Arr_REG[PC][0] == "3":
        if len(str(AC))==1:
            AC = "000"+str(AC)
        elif len(str(AC))==2:
            AC = "00"+str(AC)
        elif len(str(AC))==3:
            AC = "0"+str(AC)
        else:
            AC = str(AC)
        Arr_REG[int(Arr_REG[PC][1:])] = AC
        RESULT[3] = Arr_REG
    if Arr_REG[PC][0] == "4":
        _AC = AC[2:]
        if len(str(_AC))==1:
            _AC = "000"+str(_AC)
        elif len(str(_AC))==2:
            _AC = "00"+str(_AC)
        elif len(str(_AC))==3:
            _AC = "0"+str(_AC)
        else:
            _AC = str(_AC)
        Arr_REG[int(Arr_REG[PC][1:])-1] = _AC
        RESULT[3] = Arr_REG
    if Arr_REG[PC][0] == "5":
        _AC = Arr_REG[int(Arr_REG[PC][1:])]
        RESULT[0] = _AC
    if Arr_REG[PC][0] == "6":
        address = int(Arr_REG[PC][1:])
        RESULT[1] = int(address)
    elif Arr_REG[PC][0] == "7":
        if (int(AC) == 0):
            address = int(Arr_REG[PC][1:])
            RESULT[1] = int(address)
    elif Arr_REG[PC][0] == "8":
        if (int(AC) >= 0):
            address = int(Arr_REG[PC][1:])
            RESULT[1] = int(address)
    RESULT[2] = Arr_REG[RESULT[1]-1]
    return RESULT
def save(Arr_REG,Arr_Asm,ac_reg,pc_reg,ir_reg,INP_file="elmc_file"):
    fw = open(str(INP_file)+".elmc", "w")
    fw.write(str(ac_reg)+"/")
    fw.write(str(pc_reg)+"/")
    fw.write(str(ir_reg)+"/")
    for i in range(len(Arr_REG)):
        fw.write(str(Arr_REG[i]+"_"))
    fw.write("/")
    for j in range(len(Arr_Asm)):
        fw.write(str(Arr_Asm[j]+"_"))
    fw.write("/")
    fw.close()
    return "Save file successfully"
def restore(INP_file="elmc_file"):
    file_ = open(str(INP_file)+".elmc", "r")
    data = file_.read()
    data_list = [data.split("/") for i in range(0, len(data))][0]
    ac_reg = data_list[0]
    pc_reg = int(data_list[1])
    ir_reg = data_list[2]
    Arr_REG = [data_list[3].split("_") for j in range(0, len(data_list[3]))][0]
    Arr_Asm = [data_list[4].split("_") for j in range(0, len(data_list[4]))][0]
    return [Arr_REG[:-1],Arr_Asm[:-1],ac_reg,pc_reg,ir_reg]
#Initial 
size = 30
arr_REG=['0000']*size
arr_Asm=[]
cmd_Program = 1
ac_reg = '0000'
pc_reg = 0
ir_reg = 0
#Loop Program
print("*** Welcome to an Extended Little Man Computer Emulator! ***")
while(cmd_Program):
    _CMD_inp = input("LMC ?> ")
    CMD_inp = _CMD_inp.lower()
    if(CMD_inp == "quit" or CMD_inp == "exit"):
        cmd_Program = 0
    elif(CMD_inp == "dump"):
        dump(arr_REG,arr_Asm,ac_reg,pc_reg,ir_reg)
    elif(CMD_inp == "load"):
        result = load(arr_REG)
        arr_REG = result[0]
        arr_Asm += result[1]
    elif(CMD_inp == "step"):
        result = step(arr_REG,ac_reg,pc_reg,ir_reg)
        ac_reg = result[0]
        pc_reg = result[1]
        ir_reg = result[2]
        arr_REG = result[3]
    elif(CMD_inp == "run"):
        ac_reg = '0000'
        pc_reg = 0
        ir_reg = 0
        if len(arr_Asm)>0:
            for q in range(len(arr_Asm)):
                if (arr_Asm[pc_reg] != "STOP"):
                    result = step(arr_REG,ac_reg,pc_reg,ir_reg)
                    ac_reg = result[0]
                    pc_reg = result[1]
                    ir_reg = result[2]
                    arr_REG = result[3]
                elif (arr_Asm[pc_reg] == "STOP"):
                    result = step(arr_REG,ac_reg,pc_reg,ir_reg)
                    ac_reg = result[0]
                    pc_reg = result[1]
                    ir_reg = result[2]
                    arr_REG = result[3]
                    print('Run Complete!!!')
                    break
        else:
            print('Please load data!!!')
    elif(CMD_inp == "reset"):
        arr_REG=['0000']*size
        arr_Asm=[]
        ac_reg = 0
        pc_reg = 0
        ir_reg = 0
    elif(CMD_inp == "save"):
        fileNameSave = input("Enter file name : ")
        save(arr_REG,arr_Asm,ac_reg,pc_reg,ir_reg,fileNameSave)
        print("Save complete!!!")
    elif(CMD_inp == "restore"):
        fileNameRestore = input("Enter file name : ")
        data = restore(fileNameRestore)
        if (len(data)>0):
            arr_REG=['0000']*size
            arr_Asm=[]
            ac_reg = 0
            pc_reg = 0
            ir_reg = 0
            arr_REG=data[0]
            arr_Asm=data[1]
            ac_reg = data[2]
            pc_reg = data[3]
            ir_reg = data[4]
            print("Restore successfully!!!!")
        else:
            print("Restore Failed!!!!")
    else:
        print("'"+CMD_inp+"' is not recognized as an internal or external command\n,operable program or batch file.")
        continue