class State:
    def __init__(self,state):
        self.state = state
    def next(self,newstate):
        self.state = newstate
    def __str__(self):
        return str(self.state)
class ConcreteState(State):
    def __init__(self,state):
        self.state = State(state)
        self.NumCharComment = 0
        self.NumLineComment = 0
        self.NumLineSource = 0
    def handle(self,inp,index,_inp,eof=0):
        soa = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-.:;<=>?@[\\]^_`{|}~ \t\r\x0b\x0c'
        eol = "\n"
        slash = "/"
        if str(self.state) == "state0":
            if (str(index) == eof):
                self.state.next("final_state")
                self.NumLineSource += 1
            elif (inp in soa):
                self.state.next("state0")
            elif (inp == slash):
                self.state.next("state1")
            elif (inp == eol):
                self.state.next("state0")
                if (_inp[index-1]!=eol and _inp[index-1] != soa[93]):
                    self.NumLineSource += 1
            else:
                print("Error in State0")
        elif str(self.state) == "state1":
            if (str(index) == eof):
                self.state = State("final_state")
            elif (inp in soa):
                self.state = State("state0")
            elif (inp == slash):
                self.state = State("state2")
                self.NumLineComment += 1
            else:
                print("Error in State1")
        elif str(self.state) == "state2":
            if (str(index) == eof):
                self.state = State("final_state")
            elif (inp in soa):
                if (((_inp[index] != soa[93] and _inp[index] != eol) and _inp[index] != eof)):
                    self.NumCharComment += 1
                self.state = State("state2")
            elif (inp == slash):
                self.state = State("state2")
            elif (inp == eol):
                self.state = State("state0")
            else:
                print("Error in State2")
class Context:
    def __init__(self):
        state0 = ConcreteState("state0")
        self.state = state0
    def request(self,_input,index,_inp,eof):
        self.state.handle(_input,index,_inp,eof)
    def NumCharComment(self):
        return self.state.NumCharComment
    def NumLineComment(self):
        return self.state.NumLineComment
    def NumLineSource(self):
        return self.state.NumLineSource
import io
def loadContent(INP_file="source"):
    with io.open("source.cpp", 'r', encoding='utf8') as f:
        text = f.read()
        f.close()
    return text
input_ = loadContent()
eof = str(len(input_)-1)
index = 0
DFA = Context()
while str(DFA.state) != "final_state":
    DFA.request(input_[index],index,input_,eof)
    if(str(DFA.state) != "final_state"):
        index += 1
print("the number of characters in the comment : ",DFA.NumCharComment())
print("the number of comment lines : ",DFA.NumLineComment())
print("the number of source line : ",DFA.NumLineSource())