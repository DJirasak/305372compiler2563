import io
def loadContent(INP_file="source"):
    with io.open("source.cpp", 'r', encoding='utf8') as f:
        text = f.read()
        f.close()
    return text
def counter(arrN,Q,INP,i):
    if ((Q == "state2" and INP[i] != " ") and INP[i] != "\n"):
        arrN[0] += 1
    if (Q == "state2" and (INP[i-1] == "/" and INP[i-2] == "/")):
        arrN[1] += 1
    if ((Q == "state0" or Q == "final_state") and ( i == len(INP)-1 or (INP[i] == "\n" and INP[i] != " "))):
        arrN[2] += 1
    return arrN
#Initial
current_state = "state0"
input_ = loadContent()
eof = str(len(input_)-1)
index = 0
NumCharComment = 0
NumLineComment = 0
NumLineSource = 0
eof = str(len(input_)-1)
Q = {"state0":{" " : 'state0','0': 'state0', '1': 'state0', '2': 'state0', '3': 'state0', '4': 'state0', '5': 'state0', '6': 'state0', '7': 'state0', '8': 'state0', '9': 'state0', 'a': 'state0', 'b': 'state0', 'c': 'state0', 'd': 'state0', 'e': 'state0', 'f': 'state0', 'g': 'state0', 'h': 'state0', 'i': 'state0', 'j': 'state0', 'k': 'state0', 'l': 'state0', 'm': 'state0', 'n': 
'state0', 'o': 'state0', 'p': 'state0', 'q': 'state0', 'r': 'state0', 's': 'state0', 't': 'state0', 'u': 'state0', 'v': 'state0', 'w': 'state0', 'x': 'state0', 'y': 'state0', 'z': 'state0', 'A': 'state0', 'B': 'state0', 'C': 'state0', 'D': 'state0', 'E': 'state0', 'F': 'state0', 'G': 'state0', 'H': 'state0', 'I': 'state0', 'J': 'state0', 'K': 'state0', 'L': 'state0', 'M': 'state0', 'N': 'state0', 'O': 'state0', 'P': 'state0', 'Q': 'state0', 'R': 'state0', 'S': 'state0', 'T': 'state0', 'U': 'state0', 'V': 'state0', 'W': 'state0', 'X': 'state0', 'Y': 'state0', 'Z': 'state0', '!': 
'state0', '"': 'state0', '#': 'state0', '$': 'state0', '%': 'state0', '&': 'state0', "'": 'state0', '(': 'state0', ')': 'state0', '*': 'state0', '+': 'state0', ',': 'state0', '-': 'state0', '.': 'state0', ':': 'state0', ';': 'state0', '<': 'state0', '=': 'state0', '>': 'state0', '?': 'state0', '@': 'state0', '[': 'state0', '\\': 'state0', ']': 'state0', '^': 'state0', '_': 'state0', '`': 'state0', '{': 'state0', '|': 'state0', '}': 'state0', '~': 'state0',"\n":"state0","/":"state1"},
    "state1":{" " : 'state0','0': 'state0', '1': 'state0', '2': 'state0', '3': 'state0', '4': 'state0', '5': 'state0', '6': 'state0', '7': 'state0', '8': 'state0', '9': 'state0', 'a': 'state0', 'b': 'state0', 'c': 'state0', 'd': 'state0', 'e': 'state0', 'f': 'state0', 'g': 'state0', 'h': 'state0', 'i': 'state0', 'j': 'state0', 'k': 'state0', 'l': 'state0', 'm': 'state0', 'n': 
'state0', 'o': 'state0', 'p': 'state0', 'q': 'state0', 'r': 'state0', 's': 'state0', 't': 'state0', 'u': 'state0', 'v': 'state0', 'w': 'state0', 'x': 'state0', 'y': 'state0', 'z': 'state0', 'A': 'state0', 'B': 'state0', 'C': 'state0', 'D': 'state0', 'E': 'state0', 'F': 'state0', 'G': 'state0', 'H': 'state0', 'I': 'state0', 'J': 'state0', 'K': 'state0', 'L': 'state0', 'M': 'state0', 'N': 'state0', 'O': 'state0', 'P': 'state0', 'Q': 'state0', 'R': 'state0', 'S': 'state0', 'T': 'state0', 'U': 'state0', 'V': 'state0', 'W': 'state0', 'X': 'state0', 'Y': 'state0', 'Z': 'state0', '!': 
'state0', '"': 'state0', '#': 'state0', '$': 'state0', '%': 'state0', '&': 'state0', "'": 'state0', '(': 'state0', ')': 'state0', '*': 'state0', '+': 'state0', ',': 'state0', '-': 'state0', '.': 'state0', ':': 'state0', ';': 'state0', '<': 'state0', '=': 'state0', '>': 'state0', '?': 'state0', '@': 'state0', '[': 'state0', '\\': 'state0', ']': 'state0', '^': 'state0', '_': 'state0', '`': 'state0', '{': 'state0', '|': 'state0', '}': 'state0', '~': 'state0',"\n":"state0","/":"state2"},
    "state2":{" " : 'state2','0': 'state2', '1': 'state2', '2': 'state2', '3': 'state2', '4': 'state2', '5': 'state2', '6': 'state2', '7': 'state2', '8': 'state2', '9': 'state2', 'a': 'state2', 'b': 'state2', 'c': 'state2', 'd': 'state2', 'e': 'state2', 'f': 'state2', 'g': 'state2', 'h': 'state2', 'i': 'state2', 'j': 'state2', 'k': 'state2', 'l': 'state2', 'm': 'state2', 'n': 
'state2', 'o': 'state2', 'p': 'state2', 'q': 'state2', 'r': 'state2', 's': 'state2', 't': 'state2', 'u': 'state2', 'v': 'state2', 'w': 'state2', 'x': 'state2', 'y': 'state2', 'z': 'state2', 'A': 'state2', 'B': 'state2', 'C': 'state2', 'D': 'state2', 'E': 'state2', 'F': 'state2', 'G': 'state2', 'H': 'state2', 'I': 'state2', 'J': 'state2', 'K': 'state2', 'L': 'state2', 'M': 'state2', 'N': 'state2', 'O': 'state2', 'P': 'state2', 'Q': 'state2', 'R': 'state2', 'S': 'state2', 'T': 'state2', 'U': 'state2', 'V': 'state2', 'W': 'state2', 'X': 'state2', 'Y': 'state2', 'Z': 'state2', '!': 
'state2', '"': 'state2', '#': 'state2', '$': 'state2', '%': 'state2', '&': 'state2', "'": 'state2', '(': 'state2', ')': 'state2', '*': 'state2', '+': 'state2', ',': 'state2', '-': 'state2', '.': 'state2', ':': 'state2', ';': 'state2', '<': 'state2', '=': 'state2', '>': 'state2', '?': 'state2', '@': 'state2', '[': 'state2', '\\': 'state2', ']': 'state2', '^': 'state2', '_': 'state2', '`': 'state2', '{': 'state2', '|': 'state2', '}': 'state2', '~': 'state2',"\n":"state0","/":"state2"}
}
while True :
    result = counter([NumCharComment,NumLineComment,NumLineSource],current_state,input_,index)
    NumCharComment = result[0]
    NumLineComment = result[1]
    NumLineSource = result[2]
    if current_state == "final_state":
        print("the number of characters in the comment :", NumCharComment)
        print("the number of comment lines             :", NumLineComment)
        print("the number of source line               :", NumLineSource)
        break
    else:
        if index == int(eof)-1: 
            current_state = "final_state"
        else:
            current_state = Q[current_state][input_[index]]
        if index != int(eof): 
            index += 1