import io
def loadContent(INP_file="source"):
    with io.open("source.cpp", 'r', encoding='utf8') as f:
        text = f.read()
        f.close()
    return text
#Initial
current_state = "state0"
soa = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-.:;<=>?@[\\]^_`{|}~ \t\r\x0b\x0c'
eof = ""
eol = "\n"
slash = "/"
input_ = loadContent()
eof = str(len(input_)-1)
index = 0
NumCharComment = 0
NumLineComment = 0
NumLineSource = 0
#Loop Program
while True:
    if current_state == "state0":
        if (str(index) == eof):
            current_state = "final_state"
            NumLineSource += 1
        elif (input_[index] in soa):
            current_state = "state0"
            index += 1
        elif (input_[index] == slash):
            current_state = "state1"
            index += 1
        elif (input_[index] == eol):
            current_state = "state0"
            if (input_[index-1]!=eol and input_[index-1] != soa[93]):
                NumLineSource += 1
            index += 1
        else:
            print("Error in State0")
            break     
    elif current_state == "state1":
        if (str(index) == eof):
            current_state = "final_state"
        elif (input_[index] in soa):
            current_state = "state0"
            index += 1
        elif (input_[index] == slash):
            current_state = "state2"
            NumLineComment += 1
            index += 1
        else:
            print("Error in State1")
            break
    elif current_state == "state2":
        if (str(index) == eof):
            current_state = "final_state"
        elif (input_[index] in soa):
            if (((input_[index] != soa[93] and input_[index] != eol) and input_[index] != eof)):
                NumCharComment += 1
            current_state = "state2"
            index += 1
        elif (input_[index] == slash):
            current_state = "state2"
            index += 1
        elif (input_[index] == eol):
            current_state = "state0"
            index += 1
        else:
            print("Error in State2")
            break
    elif current_state == "final_state":
        print("the number of characters in the comment :", NumCharComment)
        print("the number of comment lines             :", NumLineComment)
        print("the number of source line               :", NumLineSource)
        break
    else:
        print("Error")
        break