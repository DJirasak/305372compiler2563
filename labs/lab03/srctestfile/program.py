#def semantic_analysis(int)===>boolean
def semantic_analysis(value):
    return True if (0 <= value <= 2147483647) else False

#def syntex_analysis(str)===>boolean
def syntax_analysis(value):
    Number=['0','1','2','3','4','5','6','7','8','9']
    for i in range(len(value)): 
        if(value[i] not in Number): 
            return False
    return True
#def TargetCodeGen(str,str,str)===>file
def TargetCodeGen(data,fileNameOutput,Target):
    fw = open("output_"+str(fileNameOutput)+"."+str(Target), "w")
    fw.write(data)
    fw.close()

"""implement simple semantic analysis to all programs"""
# Program 1.1 Src file to txt file.
def src2txt(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    if(syntax_analysis(value)):
        if (semantic_analysis(int(value))):
            TargetCodeGen(value,fileName,"txt")
            return "successes"
        else:
            return "Semantic_analysis | Value is out of the specified range"
    else:
        return "Syntax_analysis | Value not has only number"
    
# Program 1.2 Src file to py file and The output file should be executable by a Python 3 interpreter.
def src2py(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    if(syntax_analysis(value)):
        if (semantic_analysis(int(value))):
            statement  = "value = "+value+"\n"
            statement += "print(value)"
            TargetCodeGen(statement,fileName,"py")
            return "successes"
        else:
            return "Semantic_analysis | Value is out of the specified range"
    else:
        return "Syntax_analysis | Value not has only number"
    
    
# Program 1.3 Src file to c file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2c(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    if(syntax_analysis(value)):
        if (semantic_analysis(int(value))):
            statement  = "#include <stdio.h> \n"
            statement += "int main(){ \n"
            statement += "int value = "+value+";\n"
            statement += 'printf(\"%d\",value); \n'
            statement += "return 0;\n"
            statement += "}"
            TargetCodeGen(statement,fileName,"c")
            return "successes"
        else:
            return "Semantic_analysis | Value is out of the specified range"
    else:
        return "Syntax_analysis | Value not has only number"
    

# Program 1.4 Src file to cpp file and the program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cpp(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    if(syntax_analysis(value)):
        if (semantic_analysis(int(value))):
            statement  = "#include <iostream> \n"
            statement += "int main(){ \n"
            statement += "int value="+value+";\n"
            statement += 'std::cout << value; \n'
            statement += "return 0;\n"
            statement += "}"
            TargetCodeGen(statement,fileName,"cpp")
            return "successes"
        else:
            return "Semantic_analysis | Value is out of the specified range"
    else:
        return "Syntax_analysis | Value not has only number"

# Program 1.5 Src file to cs file and the executable program generated from the compiler \
# once executed should print the number originally in .src file to the screen.
def src2cs(fileName):
    f = open(str(fileName)+".src", "r")
    value = f.read()
    f.close()
    if(syntax_analysis(value)):
        if (semantic_analysis(int(value))):
            statement  = 'using System;\n'
            statement += 'public class Program {\n'
            statement += 'public static void Main(){\n'
            statement += 'int value='+value+';\n'
            statement += 'Console.WriteLine(value);\n'
            statement += '} } \n'
            TargetCodeGen(statement,fileName,"cs")
            return "successes"
        else:
            return "Semantic_analysis | Value is out of the specified range"
    else:
        return "Syntax_analysis | Value not has only number"
    
"""
print(src2txt('Xsource'))
print(src2py('Xsource'))
print(src2c('Xsource'))
print(src2cpp('Xsource'))
print(src2cs('Xsource'))"""